let gulp = require('gulp');
let replace = require('gulp-replace');
let fs = require('fs');
let git = require('gulp-git');

/**
 * Get the version from the package.json file.
 */
let getPackageVersion = function () {
  return JSON.parse(fs.readFileSync('./package.json', 'utf8')).version;
};

/**
 * Update the version in the version TypeScript file ('./src/app/app.module.ts').
 */
gulp.task('replace-version-ts', function () {
  return gulp.src(['./src/app/app.module.ts'])
    .pipe(replace(/return { version: '(.*)'/g, 'return { version: \'' + getPackageVersion() + '\''))
    .pipe(gulp.dest('./src/app/'));
});

/**
 * Commit the new version.
 */
gulp.task('commit-changes', function () {
  return gulp.src('.')
    .pipe(git.add())
    .pipe(git.commit('🔖 VERSION ' + getPackageVersion()));
});

/**
 * Task to update the version from the package.json in the others files and to commit the changes.
 */
gulp.task('update-version', gulp.series(
    'replace-version-ts',
    'commit-changes',
    function (done) {
      done();
    })
);
