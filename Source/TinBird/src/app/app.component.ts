import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NGXLogger } from 'ngx-logger';

import { AppVersionConfig } from './utils';

@Component({
  selector: 'tbr-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'TinBirdApp';

  constructor(private appVersionConfig: AppVersionConfig, private translate: TranslateService, private logger: NGXLogger) {}

  ngOnInit(): void {
    this.logger.info(`AppComponent.constructor(): Version: ${this.appVersionConfig.version}`);
    this.translate.setDefaultLang('fr');
    this.translate.use('fr');
  }
}
