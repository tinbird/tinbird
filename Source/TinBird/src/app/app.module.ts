import { DatePipe } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { LoggerModule, NgxLoggerLevel } from 'ngx-logger';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {
  ChoreographyComponent,
  CourtshipComponent,
  DressingComponent,
  LossComponent,
  MatchComponent,
  ProfileComponent,
  ProfilesComponent,
  StartComponent,
} from './components';
import { AppVersionConfig } from './utils';

/**
 * The app version.
 */
export function appVersionConfigFactory() {
  return { version: '1.1.0' };
}

/**
 * Create a translation loader.
 * AoT requires an exported function for factories.
 *
 * @param http the http client
 * @function Object() { [native code] }
 */
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    StartComponent,
    ProfilesComponent,
    ProfileComponent,
    CourtshipComponent,
    DressingComponent,
    ChoreographyComponent,
    MatchComponent,
    LossComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LoggerModule.forRoot({
      level: NgxLoggerLevel.DEBUG,
      timestampFormat: 'HH:mm:ss.SSS',
    }),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
  ],
  providers: [DatePipe, { provide: AppVersionConfig, useFactory: appVersionConfigFactory }],
  bootstrap: [AppComponent],
})
export class AppModule {}
