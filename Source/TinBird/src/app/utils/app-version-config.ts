/**
 * Configure the app version.
 */
export class AppVersionConfig {
  version: string | undefined;
}
