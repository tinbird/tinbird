export enum AppRoute {
  Start = 'start',
  Profiles = 'profiles',
  Profile = 'profile',
  Courtship = 'courtship',
  Dressing = 'dressing',
  Choreography = 'choreography',
  Match = 'match',
  Loss = 'loss',
}
