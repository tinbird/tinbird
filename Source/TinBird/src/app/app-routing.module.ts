import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {
  ChoreographyComponent,
  CourtshipComponent,
  DressingComponent,
  LossComponent,
  MatchComponent,
  ProfileComponent,
  ProfilesComponent,
  StartComponent,
} from './components';
import { AppRoute } from './utils';

const routes: Routes = [
  { path: '', redirectTo: AppRoute.Start, pathMatch: 'full' },
  { path: AppRoute.Start, component: StartComponent },
  { path: AppRoute.Profiles, component: ProfilesComponent },
  { path: `${AppRoute.Profile}/:id`, component: ProfileComponent },
  { path: `${AppRoute.Courtship}/:id`, component: CourtshipComponent },
  { path: `${AppRoute.Dressing}/:id`, component: DressingComponent },
  { path: `${AppRoute.Choreography}/:id`, component: ChoreographyComponent },
  { path: `${AppRoute.Match}/:id`, component: MatchComponent },
  { path: `${AppRoute.Loss}/:id`, component: LossComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
