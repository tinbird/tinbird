import { Injectable, OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NGXLogger } from 'ngx-logger';
import { Subscription } from 'rxjs';

import { AppRoute } from '../utils';

@Injectable({
  providedIn: 'root',
})
export class SpeciesService implements OnDestroy {
  /**
   * The array of species ids.
   */
  public ids: string[] = [];

  /**
   * The subscription array.
   *
   * @private
   */
  private subscriptions: Subscription[] = [];

  /**
   * Get the title key.
   *
   * @param id the species
   */
  public static titleKey(id: string | undefined): string {
    return `species.${id}.name`;
  }

  /**
   * Get the photo URL.
   *
   * @param id the species
   */
  public static photoUrl(id: string | undefined): string {
    return `assets/photos//${id}.jpg`;
  }

  /**
   * Get the contents keys.
   *
   * @param id the species
   */
  public static contentKeys(id: string | undefined): any {
    return `species.${id}.profile.contents`;
  }

  /**
   * Get the next route.
   *
   * @param id the species
   * @param currentRoute the current page  route
   * @param match <code>true</code> if the choreography match
   */
  public static nextRoute(id: string | undefined, currentRoute: AppRoute, match = true): string {
    switch (currentRoute) {
      case AppRoute.Start:
        return `/${AppRoute.Profiles}`;
      case AppRoute.Profile:
        return `/${AppRoute.Courtship}/${id}`;
      case AppRoute.Courtship:
        return `/${AppRoute.Dressing}/${id}`;
      case AppRoute.Dressing:
        return `/${AppRoute.Choreography}/${id}`;
      case AppRoute.Choreography:
        return `/${match ? AppRoute.Match : AppRoute.Loss}/${id}`;
      case AppRoute.Match:
      case AppRoute.Loss:
        return `/${AppRoute.Start}`;
    }
    return '';
  }

  constructor(private translate: TranslateService, private logger: NGXLogger) {
    this.logger.debug('SpeciesService.constructor()');

    this.subscriptions.push(this.translate.get('species').subscribe(species => (this.ids = Object.keys(species))));
  }

  ngOnDestroy(): void {
    this.logger.debug('SpeciesService.ngOnDestroy()');

    // Remove the subscriptions
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }
}
