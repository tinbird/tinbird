import { Component, OnDestroy, OnInit } from '@angular/core';

import { AppRoute } from '../../utils';
import { VideoComponent } from '../video/video.component';

@Component({
  templateUrl: './courtship.component.html',
})
export class CourtshipComponent extends VideoComponent implements OnInit, OnDestroy {
  protected currentRoute = AppRoute.Courtship;
}
