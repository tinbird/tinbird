import { Component } from '@angular/core';

import { AppRoute } from '../../utils';
import { StepComponent } from '../step/step.component';

@Component({
  selector: 'tbr-profile',
  templateUrl: './profile.component.html',
})
export class ProfileComponent extends StepComponent {
  protected currentRoute = AppRoute.Profile;
}
