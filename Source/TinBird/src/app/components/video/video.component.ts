import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NGXLogger } from 'ngx-logger';
import { Observable, map } from 'rxjs';
import videojs from 'video.js';

import { StepComponent } from '../step/step.component';

/**
 * The generic video component using video.js to play a video.
 */
@Component({
  template: '',
})
export abstract class VideoComponent extends StepComponent implements OnInit, OnDestroy, AfterViewInit {
  /**
   * Get the video HTML/DOM native element.
   *
   * @param element the Angular element
   * @private
   */
  @ViewChild('video') private set videoElement(element: ElementRef) {
    this.video = element.nativeElement;
  }

  /**
   * The video source HLS .m3u8 URL.
   *
   * @protected
   */
  private videoSrc: string | undefined;

  /**
   * The video HTML/DOM native element.
   *
   * @private
   */
  private video: HTMLVideoElement | undefined = undefined;

  /**
   * The video.js player.
   *
   * @private
   */
  private player: videojs.Player | undefined;

  constructor(
    protected override route: ActivatedRoute,
    protected router: Router,
    protected translate: TranslateService,
    protected override logger: NGXLogger,
  ) {
    super(route, logger);
  }

  override ngOnInit(): void {
    this.logger.debug('VideoComponent.ngOnInit()');
    super.ngOnInit();

    if (this.id) {
      this.subscriptions.push(this.initVideoSrc.subscribe(() => this.initVideo()));
    } else {
      this.subscriptions.push(
        this.idChange.subscribe(id => {
          if (id) {
            this.subscriptions.push(this.initVideoSrc.subscribe(() => this.initVideo()));
          }
        }),
      );
    }

    window.scroll(0, 0);
  }

  override ngOnDestroy(): void {
    this.logger.debug('VideoComponent.ngOnDestroy()');

    // Dispose the video
    this.player?.dispose();

    // Remove the subscriptions
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  /**
   * Create the video.js player and add the event watcher.
   */
  ngAfterViewInit(): void {
    this.logger.debug('VideoComponent.ngAfterViewInit()');

    this.initVideo();
  }

  /**
   * Initialize the video source URL.
   *
   * @private
   */
  private get initVideoSrc(): Observable<string> {
    return this.translate.get(`species.${this.id}.${this.currentRoute}`).pipe(map((value: string) => (this.videoSrc = value)));
  }

  /**
   * Initialize the video.js component.
   *
   * @private
   */
  private initVideo(): void {
    if (this.video && this.videoSrc && !this.player) {
      this.player = videojs(this.video, {
        autoplay: true,
        controls: false,
        sources: [{ src: this.videoSrc, type: 'application/x-mpegURL' }],
      });
      this.player.on('ended', () => this.next());
    }
  }

  /**
   * Navigate to the next route.
   */
  public next() {
    this.logger.debug('VideoComponent.next()');

    this.router.navigate([this.nextRoute], { relativeTo: this.route });
  }
}
