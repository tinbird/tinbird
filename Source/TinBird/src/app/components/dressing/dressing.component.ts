import { Component } from '@angular/core';

import { AppRoute } from '../../utils';
import { StepComponent } from '../step/step.component';

@Component({
  selector: 'tbr-dressing',
  templateUrl: './dressing.component.html',
})
export class DressingComponent extends StepComponent {
  protected currentRoute = AppRoute.Dressing;
}
