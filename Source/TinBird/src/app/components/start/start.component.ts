import { Component } from '@angular/core';

import { AppRoute } from '../../utils';
import { StepComponent } from '../step/step.component';

@Component({
  selector: 'tbr-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.scss'],
})
export class StartComponent extends StepComponent {
  protected currentRoute = AppRoute.Start;
}
