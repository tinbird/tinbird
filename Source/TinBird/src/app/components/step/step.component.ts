import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NGXLogger } from 'ngx-logger';
import { Subscription } from 'rxjs';

import { SpeciesService } from '../../services';
import { AppRoute } from '../../utils';

@Component({
  selector: 'tbr-step',
  template: '',
})
export abstract class StepComponent implements OnInit, OnDestroy {
  /**
   * The internal profile id (species).
   *
   * @private
   */
  private internalId: string | undefined;

  /**
   * Emit an event when.
   */
  protected idChange = new EventEmitter<string | undefined>();

  /**
   * The current page route.
   *
   * @protected
   */
  protected abstract currentRoute: AppRoute;

  /**
   * The subscription array.
   *
   * @private
   */
  protected subscriptions: Subscription[] = [];

  constructor(protected route: ActivatedRoute, protected logger: NGXLogger) {
    this.logger.debug('StepComponent.constructor()');
  }

  ngOnInit(): void {
    this.logger.debug('StepComponent.ngOnInit()');

    this.subscriptions.push(
      this.route.params.subscribe(params => {
        this.internalId = params['id'];
        this.idChange.emit(this.internalId);
      }),
    );

    window.scroll(0, 0);
  }

  ngOnDestroy(): void {
    this.logger.debug('StepComponent.ngOnDestroy()');

    // Remove the subscriptions
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  /**
   * Get the profile id (species).
   */
  public get id(): string | undefined {
    return this.internalId;
  }

  /**
   * Get the next route.
   */
  public get nextRoute(): string {
    return SpeciesService.nextRoute(this.id, this.currentRoute);
  }

  /**
   * Get the title key.
   */
  public get titleKey(): string {
    return SpeciesService.titleKey(this.id);
  }

  /**
   * Get the photo URL.
   */
  public get photoUrl(): string {
    return SpeciesService.photoUrl(this.id);
  }

  /**
   * Get the contents keys.
   */
  public get contentKeys(): any {
    return SpeciesService.contentKeys(this.id);
  }
}
