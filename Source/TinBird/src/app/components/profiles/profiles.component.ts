import { Component } from '@angular/core';

import { SpeciesService } from '../../services';
import { AppRoute } from '../../utils';

@Component({
  selector: 'tbr-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.scss'],
})
export class ProfilesComponent {
  public ProfilesComponent = ProfilesComponent;
  public SpeciesService = SpeciesService;
  constructor(public species: SpeciesService) {}

  public static nextRoute(id: string): string {
    return `../${AppRoute.Profile}/${id}`;
  }
}
