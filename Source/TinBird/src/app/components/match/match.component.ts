import { Component } from '@angular/core';

import { AppRoute } from '../../utils';
import { StepComponent } from '../step/step.component';

@Component({
  selector: 'tbr-match',
  templateUrl: './match.component.html',
})
export class MatchComponent extends StepComponent {
  protected currentRoute = AppRoute.Match;
}
