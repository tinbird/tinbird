import { Component } from '@angular/core';

import { AppRoute } from '../../utils';
import { StepComponent } from '../step/step.component';

@Component({
  selector: 'tbr-loss',
  templateUrl: './loss.component.html',
})
export class LossComponent extends StepComponent {
  protected currentRoute = AppRoute.Loss;
}
