import { Component, OnDestroy, OnInit } from '@angular/core';

import { environment } from '../../../environments/environment';
import { SpeciesService } from '../../services';
import { AppRoute } from '../../utils';
import { VideoComponent } from '../video/video.component';

@Component({
  templateUrl: './choreography.component.html',
})
export class ChoreographyComponent extends VideoComponent implements OnInit, OnDestroy {
  protected currentRoute = AppRoute.Choreography;

  public override get nextRoute(): string {
    return this.matchRoute(Math.random() <= environment.match);
  }

  /**
   * Get the next route.
   *
   * @param match <code>true</code> if the match is positive
   */
  public matchRoute(match: boolean): string {
    return SpeciesService.nextRoute(this.id, this.currentRoute, match);
  }
}
